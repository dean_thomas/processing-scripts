//  Define some colour constants
final color COLOR_OUTDOOR_DENIM = color(67,81,104);
final color COLOR_BLACK = color(0,0,0);
final color COLOR_WHITE = color(255,255,255);

//  Visualization colours
final color BACKGROUND_COLOR = COLOR_OUTDOOR_DENIM;
final color CELL_COLOR = COLOR_WHITE;
final color CELL_COLOR_HIGHLIGHTED = COLOR_OUTDOOR_DENIM;

//  Depth of traversal
final int ROWS = 128;

//  Text
final int FONT_SIZE = 2;
final boolean DISPLAY_TEXT = true;
final color TEXT_COLOR = COLOR_BLACK;

class Cell
{
  int value = 1;

  Cell(int value)
  {
    this.value = value;
  }
}

class Row
{
  ArrayList<Cell> cells;
  int cellCount;
  
  Row(int cellCount)
  {
    this.cellCount = cellCount;
    cells = new ArrayList<Cell>();
    
    for (int c = 0; c < cellCount; ++c)
    {
      cells.add(new Cell(0));
    }
  }
}

class PascalsTriangleStructure
{
  ArrayList<Row> rows;
  int rowCount = 0;
  
  int baseWidth = 0;
  int pyramidHeight = 0;
  
  PascalsTriangleStructure(int rowCount)
  {
    this.rowCount = rowCount;
    rows = new ArrayList<Row>();
      
    for (int r = 1; r < rowCount+1; ++r)
    {
      rows.add(new Row(r));
    }
  
    baseWidth = CELL_WIDTH * rowCount;
    pyramidHeight = CELL_HEIGHT * rowCount;
    
    calculateValues();
  }
  
  void calculateValues()
  {
    for (int r = 0; r < rowCount; ++r)
    {
      if (r == 0)
      {
        //  Top of pyramid
        rows.get(0).cells.get(0).value = 1;
      }
      else
      {
        for (int c = 0; c < rows.get(r).cellCount; ++c)
        {
          //  Ends of row
          if ((c == 0) || (c == rows.get(r).cellCount-1))
          {
            rows.get(r).cells.get(c).value = 1;
          }
          else
          {
            //  Get the values from the two cells immediately above
            rows.get(r).cells.get(c).value = 
              rows.get(r-1).cells.get(c-1).value + rows.get(r-1).cells.get(c).value;
          }  
        }
      }  
    }
  }
}

PascalsTriangleStructure pascalsTriangle;
PFont font;
int CELL_WIDTH;
int CELL_HEIGHT;


void setup()
{
  font = createFont("Arial", FONT_SIZE, true);
  textFont(font, FONT_SIZE);

  CELL_WIDTH = (int)textWidth("000000");
  CELL_HEIGHT = (int)(CELL_WIDTH * 0.6); 
  
  pascalsTriangle = new PascalsTriangleStructure(ROWS);
  
  size(pascalsTriangle.baseWidth,pascalsTriangle.pyramidHeight);
}

void draw()
{
  background(BACKGROUND_COLOR);
  
  float centreLine = pascalsTriangle.baseWidth / 2;
  
  for (int r = 0; r < pascalsTriangle.rowCount; ++r)
  {
    float cellsInRow = pascalsTriangle.rows.get(r).cellCount;
    int cellIndex = 0;
    
    for (float c = (-cellsInRow / 2); c < (cellsInRow / 2); ++c)
    {
      drawCell(centreLine + (c*CELL_WIDTH), r*CELL_HEIGHT, pascalsTriangle.rows.get(r).cells.get(cellIndex).value);
      cellIndex++;        
    }
  } 
}

boolean isEven(int value)
{
  return (value % 2 == 0);
}

void drawCell(float x, float y, int value)
{
  if (isEven(value))
  {
    fill(CELL_COLOR_HIGHLIGHTED);
  }
  else
  {
    fill(CELL_COLOR);  
  }
  
  rect(x, y, CELL_WIDTH, CELL_HEIGHT);
  
  //  Calculate the offset for the text
  //  and draw in the centre of the cell
  fill(TEXT_COLOR);
  float y_offset = (CELL_HEIGHT / 2) + (textAscent() / 2) - 1;
    
  if ((textWidth(""+value)) < CELL_WIDTH)
  {
    float x_offset = (CELL_WIDTH / 2) - (textWidth(""+value) / 2);
    text(""+value, x + x_offset, y + y_offset);
  }
  else
  {
    float x_offset = (CELL_WIDTH / 2) - (textWidth("...") / 2);
    text("...", x + x_offset, y + y_offset);
  }
}

void mouseClicked(MouseEvent me)
{
  
}
