//  Maximum Width and height of the window
int SCREEN_WIDTH = 500;
int SCREEN_HEIGHT = 500;

//  Number of cells wide and high for the matrix
int CELLS_HIGH = 150;
int CELLS_WIDE = 150;

//  Calculate cell sizes
int CELL_WIDTH = SCREEN_WIDTH / CELLS_WIDE;
int CELL_HEIGHT = SCREEN_HEIGHT / CELLS_HIGH;

//  Define some colour constants
color RED = color(255,0,0);
color BLUE = color(0,0,255);
color WHITE = color(255, 255, 255);

//  The modular divider
int modulus = 0;
int delta = 1;
int MOD_MAX = 75;
int MOD_MIN = 1;

//  Ranges for the colour spectrum  
color SPECTRUM_LOWER = WHITE;
color SPECTRUM_HIGHER = BLUE;

//  Keep track of the number of frames
int frame = 1;

void setup()
{
  //  Remove excess borders in window
  SCREEN_WIDTH -= (SCREEN_WIDTH % CELLS_WIDE);
  SCREEN_HEIGHT -= (SCREEN_HEIGHT % CELLS_HIGH);
  
  size(SCREEN_WIDTH,SCREEN_HEIGHT);
}

void draw()
{
  updateModulus();
  
  drawGrid();
  
  exportFrame();
  
  if (frame < 150)
  {
    frame++;
  }
  else
  {
    exit(); 
  }
}

void updateModulus()
{
  modulus += delta;
  
  if (modulus <= MOD_MIN - 1)
  {
    delta = 1;
    modulus = MOD_MIN;
  }
  else if (modulus == MOD_MAX + 1)
  {
    delta = -1;
    modulus = MOD_MAX; 
  }
}

void drawGrid()
{
  for (int y = 0; y < CELLS_HIGH; ++y)
  {
    for (int x = 0; x < CELLS_WIDE; ++x)
    { 
      //  Cell value 
      int value = x*y;
  
      //  Calculate the remainder and callulate a percentage between 0 and 1
      int remainder = value % modulus;
      float percentage = map(remainder, 0, modulus, 0, 1);
      
      //  Assign the appropriate colour
      color fillColor = lerpColor(SPECTRUM_LOWER,SPECTRUM_HIGHER,percentage);
      fill(fillColor);
      
      //  Draw the cell
      drawCell(x,y); 
    }  
  }
}

void drawCell(int x, int y)
{
  rect(CELL_WIDTH*x,CELL_HEIGHT*y,CELL_WIDTH,CELL_HEIGHT);
}

void exportFrame()
{
  String frameName = "frame" + String.format("%03d", frame);
  
  saveFrame("frames/"+frameName+".png");  
}
