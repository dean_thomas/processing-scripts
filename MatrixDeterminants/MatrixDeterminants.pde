//  Matrix properties
final int MATRIX_ORDER = 2;

//  Visualization colours
final color COLOR_OUTDOOR_DENIM = color(67,81,104);
final color COLOR_BLACK = color(0,0,0);
final color COLOR_WHITE = color(255,255,255);
final color COLOR_ORANGE = color(255,122,66);

//  Text
final int FONT_SIZE = 32;
final color TEXT_COLOR = COLOR_BLACK;
final color TEXT_COLOR_HIGHLIGHT = COLOR_ORANGE;

final color BACKGROUND_COLOR = COLOR_WHITE;

//  Border offset
final int TOP_BORDER = 20;
final int LEFT_BORDER = 10;

//  Frames between animation changes
final int ANIMATION_STEP = 60;

class MatrixElement
{
  String identifier;
  int row;
  int col;  
  boolean isHighlighted;
  
  void Highlight()
  {
    isHighlighted = true; 
  }
  
  void UnHighlight()
  {
    isHighlighted = false; 
  }
  
  MatrixElement(String identifier, int row, int col)
  {
    this.identifier = identifier;
    this.row = row;
    this.col = col;
    this.isHighlighted = false;
  }
  
  void display(int x_offset, int y_offset)
  {
    if (isHighlighted)
      fill(TEXT_COLOR_HIGHLIGHT);
    else
      fill(TEXT_COLOR);
    textSize(FONT_SIZE);
    
    int identifierWidth = (int)textWidth(identifier);
    int textBaseX = x_offset + ((col-1) * elementWidth);
    int textBaseY = elementHeight + y_offset + ((row-1) * elementHeight);
    
    text(""+identifier, textBaseX, textBaseY);
    
    textSize(FONT_SIZE / 2);
    text(""+row+col, textBaseX+identifierWidth, textBaseY); 
  }
}

class Matrix
{
  //  Offset between bracket and leftmost
  //  element of the matrix
  static final int LEFT_ELEMENT_OFFSET = 10;
  static final int RIGHT_ELEMENT_OFFSET = 10;
  static final int TOP_ELEMENT_OFFSET = -5;
  
  MatrixElement[][] elements;
  String identifier;
  int rows;
  int cols;
  float pixelHeight;
  int pixelWidth;
  
  Matrix(String identifier, int rows, int cols)
  {
    this.identifier = identifier;
    this.rows = rows;
    this.cols = cols;
    
    elements = new MatrixElement[rows][cols];
  
    for (int r = 0; r < rows; ++r)
    {
      for (int c = 0; c < cols; ++c)
      {
        elements[r][c] = new MatrixElement(identifier, r+1, c+1);  
      }
    }
   
     pixelHeight = rows * elementHeight;
     pixelWidth = (cols * elementWidth) + LEFT_ELEMENT_OFFSET + RIGHT_ELEMENT_OFFSET;
  }
  
  void display(int x_offset, int y_offset)
  {
    drawBrackets(x_offset,y_offset);
    
    for (int r = 0; r < rows; ++r)
    {
      for (int c = 0; c < cols; ++c)
      {
         elements[r][c].display(x_offset + LEFT_ELEMENT_OFFSET, y_offset + TOP_ELEMENT_OFFSET);  
      }
    }
  }
  
  void drawBrackets(int x_offset, int y_offset)
  {
    fill(TEXT_COLOR);
    strokeWeight(2);
    
    //  Left bracket
    line(x_offset, y_offset, x_offset, y_offset + pixelHeight);
    
    //  Right bracket
    line(pixelWidth, y_offset, pixelWidth, y_offset + pixelHeight);
  }
}

interface IAnimationEvent
{
  void Execute();
  int GetFrame();
}

class FormulaAnimationEvent implements IAnimationEvent
{
  int frame;
  String newString;
  
  FormulaAnimationEvent(int frame, String newString)
  {
    this.frame = frame;
    this.newString = newString;
  }
  
  void Execute()
  {
    determinantFormula.formula += newString; 
  }
  
  int GetFrame()
  {
    return frame; 
  }
}

class MatrixElementAnimationEvent implements IAnimationEvent
{
  int frame;
  int row;
  int col;
  boolean highlight;
  
  MatrixElementAnimationEvent(int frame, int row, int col, boolean highlight)
  {
    this.frame = frame;
    this.row = row;
    this.col = col;
    this.highlight = highlight; 
  }
  
  int GetFrame()
  {
    return frame; 
  }
  
  void Execute()
  {
    if (highlight)
    {
      matrix.elements[row][col].Highlight();
      determinantFormula.formula += (matrix.elements[row][col].identifier + matrix.elements[row][col].row + matrix.elements[row][col].col);
    }
    else
    {
      matrix.elements[row][col].UnHighlight();
    }
  }
}

class MatrixAnimation
{
  ArrayList<IAnimationEvent> events;
 
  MatrixAnimation()
  {
    events = new ArrayList<IAnimationEvent>();
  }
 
  void AddEvent(IAnimationEvent newEvent)
  {
    events.add(newEvent);
  }
  
  void Execute()
  {
    for (int i = 0; i < events.size(); ++i)
    {
      if (events.get(i).GetFrame() == currentFrame)
      {
        events.get(i).Execute(); 
      }
    }  
  }
}

class Formula
{
  String formula;
  
  Formula()
  {
    formula = ""; 
  }
  
  void display(int x_offset, int y_offset)
  {
    textSize(FONT_SIZE);
    fill(TEXT_COLOR);
    
    text(formula, x_offset, y_offset);
  } 
}

MatrixAnimation matrixAnimation;
Matrix matrix;
Formula determinantFormula;
int elementWidth;
int elementHeight;
int currentFrame = 0;
PFont font;

void setup()
{
  //font = createFont("Monospaced.plain", FONT_SIZE, true);
  font = createFont("Gungsuh", FONT_SIZE, true);
  textFont(font, FONT_SIZE);

  textSize(FONT_SIZE);
  
  elementWidth = (int)textWidth("000");
  //elementWidth *= 2;
  elementHeight = (int)(elementWidth * 0.6);
  
  matrix = new Matrix("A", MATRIX_ORDER, MATRIX_ORDER);
  
  determinantFormula = new Formula();
  
  size(600,300);
  
  setupAnimation();
}

void setupAnimation()
{
  matrixAnimation = new MatrixAnimation();
  
  if (MATRIX_ORDER == 2)
  {
    matrixAnimation.AddEvent(new FormulaAnimationEvent(0 * ANIMATION_STEP, "det(A) = "));
    
    //  Highlight a11
    matrixAnimation.AddEvent(new FormulaAnimationEvent(1 * ANIMATION_STEP, "("));
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(1 * ANIMATION_STEP, 0, 0, true));
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(2 * ANIMATION_STEP, 0, 0, false));
    
    //  Highlight a22
    matrixAnimation.AddEvent(new FormulaAnimationEvent(3 * ANIMATION_STEP, "*"));
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(3 * ANIMATION_STEP, 1, 1, true));
    matrixAnimation.AddEvent(new FormulaAnimationEvent(3 * ANIMATION_STEP, ")"));
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(4 * ANIMATION_STEP, 1, 1, false));
    
    //  Subtraction
    matrixAnimation.AddEvent(new FormulaAnimationEvent(5 * ANIMATION_STEP, " - "));
    matrixAnimation.AddEvent(new FormulaAnimationEvent(6 * ANIMATION_STEP, "("));    
    
    //  Highlight a21
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(6 * ANIMATION_STEP, 1, 0, true));
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(7 * ANIMATION_STEP, 1, 0, false));
    
    //  Highlight a12
    matrixAnimation.AddEvent(new FormulaAnimationEvent(8 * ANIMATION_STEP, "*"));
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(8 * ANIMATION_STEP, 0, 1, true));
    matrixAnimation.AddEvent(new FormulaAnimationEvent(8 * ANIMATION_STEP, ")"));
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(9 * ANIMATION_STEP, 0, 1, false));
  }
  else if (MATRIX_ORDER == 3)
  {
    //  Positive Permutations
    //  =====================
    //  Highlight a11
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(1 * ANIMATION_STEP, 0, 0, true));
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(2 * ANIMATION_STEP, 0, 0, false));
    
    //  Highlight a22
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(2 * ANIMATION_STEP, 1, 1, true));
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(3 * ANIMATION_STEP, 1, 1, false));
    
    //  Highlight a33
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(3 * ANIMATION_STEP, 2, 2, true));
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(4 * ANIMATION_STEP, 2, 2, false));
    
    //  Highlight a12
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(4 * ANIMATION_STEP, 0, 1, true));
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(5 * ANIMATION_STEP, 0, 1, false));
    
    //  Highlight a23
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(5 * ANIMATION_STEP, 1, 2, true));
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(6 * ANIMATION_STEP, 1, 2, false));
    
    //  Highlight a31
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(6 * ANIMATION_STEP, 2, 0, true));
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(7 * ANIMATION_STEP, 2, 0, false));
    
    //  Highlight a13
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(7 * ANIMATION_STEP, 0, 2, true));
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(8 * ANIMATION_STEP, 0, 2, false));
    
    //  Highlight a21
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(8 * ANIMATION_STEP, 1, 0, true));
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(9 * ANIMATION_STEP, 1, 0, false));
    
    //  Highlight a32
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(9 * ANIMATION_STEP, 2, 1, true));
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(10 * ANIMATION_STEP, 2, 1, false));
    
    //  Negative Permutations
    //  =====================
    //  Highlight a13
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(11 * ANIMATION_STEP, 0, 2, true));
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(12 * ANIMATION_STEP, 0, 2, false));
    
    //  Highlight a22
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(12 * ANIMATION_STEP, 1, 1, true));
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(13 * ANIMATION_STEP, 1, 1, false));
    
    //  Highlight a31
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(13 * ANIMATION_STEP, 2, 0, true));
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(14 * ANIMATION_STEP, 2, 0, false));
    
    
    //  Highlight a12
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(14 * ANIMATION_STEP, 0, 1, true));
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(15 * ANIMATION_STEP, 0, 1, false));
    
    //  Highlight a21
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(15 * ANIMATION_STEP, 1, 0, true));
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(16 * ANIMATION_STEP, 1, 0, false));
    
    //  Highlight a33
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(16 * ANIMATION_STEP, 2, 2, true));
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(17 * ANIMATION_STEP, 2, 2, false));
    
    
    //  Highlight a11
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(17 * ANIMATION_STEP, 0, 0, true));
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(18 * ANIMATION_STEP, 0, 0, false));
    
    //  Highlight a23
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(18 * ANIMATION_STEP, 1, 2, true));
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(19 * ANIMATION_STEP, 1, 2, false));
    
    //  Highlight a32
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(19 * ANIMATION_STEP, 2, 1, true));
    matrixAnimation.AddEvent(new MatrixElementAnimationEvent(20 * ANIMATION_STEP, 2, 1, false));
  }
}

void draw()
{
  background(BACKGROUND_COLOR);
  
  matrixAnimation.Execute();
  
  matrix.display(LEFT_BORDER,TOP_BORDER);
  determinantFormula.display(200, 100);
  
  currentFrame++;
}
